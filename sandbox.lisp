(defparameter *random* (loop repeat 100 collect (random 10000)))

(loop for i in *random*
     counting (evenp i) into evens
     counting (oddp i) into odds
     summing i into total
     maximizing i into max
     minimizing i into min
     finally
     (format t
             "~%Min:~a, Max:~a, Total:~a, Evens:~a, Odds:~a~%"
                  min     max       total     evens    odds))
