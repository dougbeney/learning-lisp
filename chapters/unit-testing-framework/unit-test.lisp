;;;
;;; Unit Testing Framework
;;; http://www.gigamonkeys.com/book/practical-building-a-unit-test-framework.html
;;;

(defvar *test-name* NIL)

(defun report-result (result form)
  (format t "~:[FAIL~;pass~] ... ~a ~a~%" result *test-name* form)
  result)

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defmacro combine-results (&body forms)
  (with-gensyms (result)
    `(let ((,result t))
       ,@(loop for f in forms collect `(unless ,f (setf ,result nil)))
       ,result)))

;;(defmacro check (form)
;;  `(report-result ,form ',form))
(defmacro check (&body forms)
  `(combine-results
    ,@(loop for f in forms collect `(report-result ,f ',f))))

(defmacro deftest (name args &body body)
  `(defun ,name ,args
     (let ((*test-name* (append *test-name* (list ',name))))
       ,@body)))

(deftest test-arithmetic ()
  (combine-results
   (test-+)
   (test-*)))

(deftest test-+ ()
  (check
   (= (+ 1 2) 3)
   (= (+ 6 10) 16)
   (= (+ 10 33) 43)))

(deftest test-* ()
  (check
   (= (* 1 2) 2)
   (= (* 6 10) 60)
   (= (* 10 33) 330)))

