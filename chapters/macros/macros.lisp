;;;
;;; Lisp Macros
;;;

;;; Example 1: a prime number loop
(defun primep (number)
  (when (> number 1)
    (loop for fac from 2 to (isqrt number) never (zerop (mod number fac)))))

(defun next-prime (number)
  (loop for n from number when (primep n) return n))

;; How the below macro would look as a do loop
;(do ((p (next-prime 0) (next-prime (1+ p))))
;    ((> p 19))
;(format t "~d " p))
(defmacro plot ((x y))
  (format t "You plotted ~a,~a~%" x y))

(defmacro do-primes ((var start end) &body body)
  (let ((ending-value-name (gensym)))
    `(do ((,var (next-prime ,start) (next-prime (1+ ,var)))
          (,ending-value-name ,end))
         ((> ,var ,ending-value-name))
       ,@body)))

;; Trying out the macro
;; In Slime, use C-c RET to macroexpand
(do-primes (p 0 10) (print "hello"))

;;; Example 2: A macro-writing macro
(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

;; Re-writing the macro
;; It is slightly trivial but does improve readability a bit.
(defmacro do-primes ((var start end) &body body)
  (with-gensyms (ending-value-name)
    `(do ((,var (next-prime ,start) (next-prime (1+ ,var)))
          (,ending-value-name ,end))
         ((> ,var ,ending-value-name))
       ,@body)))

;; Scary once-only Example
(defmacro once-only ((&rest names) &body body)
  (let ((gensyms (loop for n in names collect (gensym))))
    `(let (,@(loop for g in gensyms collect `(,g (gensym))))
      `(let (,,@(loop for g in gensyms for n in names collect ``(,,g ,,n)))
        ,(let (,@(loop for n in names for g in gensyms collect `(,n ,g)))
           ,@body)))))
;; Hint: Macroexpand this
(defvar bob "Bob")
(defvar ted "Ted")
(once-only (bob ted) (print "hello"))

