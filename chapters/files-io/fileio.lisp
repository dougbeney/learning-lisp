;;; Reads the first line
(format t "This code will print a single line in a file:~%")
(let ((in (open "example.txt")))
  (format t "~a~%" (read-line in))
  (close in))

(format t "~%This code will print an entire file:~%")
(let ((in (open "example.txt")))
  (loop for line = (read-line in nil)
     while line do (format t "~a~%" line))
  (close in))

(format t "~%---~%Using with-open-file~%---~%")

(with-open-file (stream "example-2.txt" :direction :output :if-exists :supersede)
  (format stream "Hello world~%welcome this a test~%"))

(with-open-file (stream "example-2.txt")
  (loop for line = (read-line stream nil)
       while line do (format t "~a~%" line)))
