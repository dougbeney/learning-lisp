;;; A Simple Lisp Database. Uses many different Lisp concepts.
;;; http://www.gigamonkeys.com/book/practical-a-simple-database.html ;;;
;;; Completed by Doug Beney on 07-17-2018

(defvar *db* nil)

(defun add-record (cd) (push cd *db*))

(defun make-cd (title artist)
  (list :title title :artist artist))

(defun dump-db ()
  (dolist (cd *db*)
    (format t "~{~a:~10t~a~%~}~%" cd)))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-for-cd ()
  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")))

(defun add-cds ()
  (loop
     (add-record (prompt-for-cd))
     (if (not (y-or-n-p "Another?")) (return))))

(defun save-db (filename)
  (with-open-file (out filename :direction :output :if-exists :supersede)
    (with-standard-io-syntax
      (print *db* out))))

(defun load-db (filename)
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *db* (read in)))))

(defun select (selector-fn)
  (remove-if-not selector-fn *db*))

(defun make-comparison-expr (field value)
  `(equal (getf cd ,field) ,value))

(defun make-comparisons-list (fields)
  (loop while fields
       collecting (make-comparison-expr (pop fields) (pop fields))))

;;bad way
;; (defun where (&key title artist)
;;   #'(lambda (cd)
;;     (and
;;       (if title  (equal (getf cd :title) title) t)
;;       (if artist (equal (getf cd :artist) artist) t))))

;;macro where

(defmacro where (&rest clauses)
  `#'(lambda (cd) (and ,@(make-comparisons-list clauses))))

(defun update (selector-fn &key title artist rating)
  (setf *db*
        (mapcar
         #'(lambda (row)
             (when (funcall selector-fn row)
               (if title (setf (getf row :title) title))
               (if artist (setf (getf row :artist) artist))
               (if rating (setf (getf row :rating) rating)))
             row)
         *db*)))

(defun delete-row (selector-fn)
  (setf *db* (remove-if selector-fn *db*)))
