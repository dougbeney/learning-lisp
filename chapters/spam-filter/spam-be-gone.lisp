;; Spam Filter
;; Date Started: 2018-08-10

(ql:quickload "cl-ppcre")
(load "../pathname-lib/pathname.lisp")

(defpackage :spam-filter
  (:use :common-lisp :pathnames))
(in-package :spam-filter)

(defparameter *max-ham-score* 0.4)
(defparameter *min-spam-score* 0.6)

(defvar *feature-database* (make-hash-table :test #'equal))
(defun clear-database ()
  (setf *feature-database* (make-hash-table :test #'equal)))

(defclass word-feature ()
  ((word
    :initarg :word
    :accessor word-feature-word
    :initform (error "Must supploy :word")
    :documentation "The word this feature represents")
   (spam-count
    :initarg :spam-count
    :accessor spam-count
    :initform 0
    :documentation "Number of spams we have seen this feature in.")
   (ham-count
    :initarg :ham-count
    :accessor ham-count
    :initform 0
    :documentation "Number of hams we have seen this feature in")))

(defun intern-feature (word)
  (or (gethash word *feature-database*)
      (setf (gethash word *feature-database*)
            (make-instance 'word-feature :word word))))

(defun extract-words (text)
  (delete-duplicates
   (cl-ppcre:all-matches-as-strings "[a-zA-Z]{3,}"
                                    text)
   :test #'string=))

(defun extract-features (text)
  (mapcar #'intern-feature (extract-words text)))

(defun classification (score)
  (cond
    ((<= score *max-ham-score*) 'ham)
    ((>= score *min-spam-score*) 'spam)
    (t 'unsure)))

;(defun spam-or-ham (text)
;  (classification (score (extract-features text))))

