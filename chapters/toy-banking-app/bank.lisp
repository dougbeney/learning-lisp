(defparameter *account-numbers* 0)

(defclass bank-account ()
  ((name
    :initarg :name
    :initform (error "You must specify a name"))
   (balance
    :initarg :balance
    :initform 0)
   (account-number
    :initform (incf *account-numbers*))
   (account-type
    :initform "B rand New")))

(defmethod initialize-instance :after ((account bank-account) &key)
  (let ((balance (slot-value account 'balance)))
    (setf (slot-value account 'account-type)
          (cond
            ((>= balance 100000) :gold)
            ((>= balance 50000) :silver)
            (t :bronze)))))

(defmethod initialize-instance :after ((account bank-account)
                                       &key opening-bonus-percentage)
  (when opening-bonus-percentage
    (incf (slot-value account 'balance)
          (* (slot-value account 'balance)
             (/ opening-bonus-percentage 100)))))

(defparameter *myaccount*
  (make-instance 'bank-account
                 :name "Doug"
                 :balance 1000000
                 :opening-bonus-percentage 1))

(describe *myaccount*)
